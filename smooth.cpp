#include <iostream>
#include <algorithm>
#include <omp.h>
using namespace std;

unsigned int cdata[] = {
	1, 1, 3, 5, 9, 15, 25, 41, 67, 109, 177, 287, 465, 753, 1219, 1973,
	3193, 5167, 8361, 13529, 21891, 35421, 57313, 92735, 150049, 242785,
	392835, 635621, 1028457, 1664079, 2692537, 4356617, 7049155, 11405773,
	18454929, 29860703, 48315633, 78176337, 126491971, 204668309, 331160281,
	535828591, 866988873, 1402817465, 2269806339u, 3672623805u,
};
void sorts(int* data, int c11, int i11, int* t1) {
	int prevs;
	int max1;
	int ch1;
	int ch2;
	
	while (i11 > 0) {
		prevs = c11 - cdata[t1[i11]];
		if (data[c11]>=data[prevs]) {
			break;
		}
		else
		{
			if (t1[i11] > 1) {
				ch1 = c11 - 1 - cdata[t1[i11] - 2];
				ch2 = c11 - 1;
				if (data[prevs]<data[ch1]) break;
				if (data[prevs]<data[ch2]) break;
			}
			swap(data[c11], data[prevs]);
			c11 = prevs;
			i11 -= 1;
		}
	}
	int l22 = t1[i11];
	while (l22 > 1) {
		max1 = c11;
		ch1 = c11 - 1 - cdata[l22 - 2];
		ch2 = c11 - 1;

		if (data[max1]<data[ch1]){
			max1 = ch1;
		}
		if (data[max1]<data[ch2])
		{
			max1 = ch2;
		}
		if (max1 == ch1) {
			swap(data[c11], data[ch1]);
			c11 = ch1;
			l22 -= 1;
		}
		else if (max1 == ch2) {
			swap(data[c11], data[ch2]);
			c11 = ch2;
			l22 -= 2;
		}
		else
		{
			break;
		}
	}

}

void kernalSort(int threads,int* data, int size) {

	int t1[64] = { 1 };
	int tle = 0;
	int i;
	omp_set_num_threads(threads);
#pragma omp parallel sections
{
	#pragma omp section
	for (i = 1; i < size; i++) {
	
		if (tle > 0 && t1[tle - 1] - t1[tle] == 1) {
			
			tle -= 1;
			
			t1[tle] += 1;
		}
		else if (t1[tle] != 1) {
			
			tle += 1;
			t1[tle] = 1;
		}
		else {
			
			tle += 1;
			t1[tle] = 0;
		}

		sorts(data, i, tle, t1);
	}
}	
	for (i = size - 2; i > 0; i--) {
		if (t1[tle] <= 1) {
			tle -= 1;
		}
		else {
			t1[tle] -= 1;
			t1[tle + 1] = t1[tle] - 1;
			tle += 1;
#pragma omp parallel sections
{
	#pragma omp section
			sorts(data, i - cdata[t1[tle]], tle - 1, t1);
			#pragma omp section
			sorts(data, i, tle, t1);
		}
	}
	}	
	
}
void print(int data[],int nsize){
	int i;
	for (i = 0; i<nsize; i++)
	{
		cout<<" "<<data[i]<<" ";
	}
	cout<<endl;
}
int main()
{
	int tmp = 0, i, nsize;
	int threads;
	threads = 1;
	cout<<"nsizegth:";
	cin>>nsize;
	cout<<"threads:";
	cin>>threads;
	int data[nsize];
	for (i = 0; i<nsize; i++) {
		data[i]=rand()%200+23;
	}

	print(data,nsize);
	omp_set_num_threads(threads);
	double start = omp_get_wtime();
	kernalSort(threads,data, nsize);
	double end = omp_get_wtime();
	print(data,nsize);
	cout<<"time: "<<end - start<<endl;
	
	return 0;
}
