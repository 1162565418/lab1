#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <omp.h>
using namespace std;
typedef struct BSTNode	//
{
	int data;
	BSTNode *lchild;  //
	BSTNode *rchild;  //
}BSTNode, *BSTree;
 
 
bool Search(BSTree bst, int key, BSTree f, BSTree *p)  //
{
	if (!bst)
	{
		*p = f;
		return false;
	}
	if (bst->data == key) 
	{
		*p = bst;
		return true;
	}
	else if (bst->data < key)
	{
		return Search(bst->rchild, key, bst, p);
	}
	return Search(bst->lchild, key, bst, p);
}
 
void InOderTraverse(BSTree bst)   
{
	if (NULL != bst)
	{
#pragma omp single
{
	InOderTraverse(bst->lchild);
}	
	cout<<" "<<bst->data<<" ";
#pragma omp single
{	
	InOderTraverse(bst->rchild);
}
	}
}
 
static BSTNode* BuyNode(int data)   
{
	BSTNode *pTmp = (BSTNode*)malloc(sizeof(BSTNode));
	if (NULL == pTmp)
	{
		exit(0);
	}
	pTmp->data = data;
	pTmp->lchild = NULL;
	pTmp->rchild = NULL;
	return pTmp;
}
 
bool Insert(BSTree *bst, int key)
{
	if (NULL == *bst)  
	{
		*bst = BuyNode(key);   
		return true;
	}
	BSTNode *p;
	
	if (!Search(*bst, key, NULL, &p))  
	{
		BSTNode *pNew = BuyNode(key);
		if (key < p->data) 
		{
			p->lchild = pNew;
		}
		else if (key > p->data)  
		{
			p->rchild = pNew;
		}
		return true;  
	}

	return false;
}

 
int main(void)
{
	BSTNode *root = NULL;
	int num,i,a,counts;
	cout<<"input dsize：";
	cin>>num;
	cout<<"intput thread num:";
	cin>>counts;
	
	cout<<"before：";
	for(i=0;i<num;i++)
	{
		a=rand()%232;
		cout<<" "<<a<<" ";
		Insert(&root, a);
	} 
	cout<<endl<<"after：";
	omp_set_num_threads(counts);
	double start = omp_get_wtime();
	InOderTraverse(root);//
	double end = omp_get_wtime();
	cout<<endl<<"time: "<<end-start<<endl;
	return 0;
}
